﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjectInfos", menuName = "Assets/Create/DataHolder")]
public class ObjectDataHolder : ScriptableObject
{
    [Sirenix.OdinInspector.ShowInInspector]
    public List<ObjectData> objectDatas;
}

