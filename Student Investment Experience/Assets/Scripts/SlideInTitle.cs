﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideInTitle : MonoBehaviour
{
    static public SlideInTitle instance;
    // Start is called before the first frame update
    void Awake()
    {
        titleUI.SetActive(false);

        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    [SerializeField]
    GameObject titleUI;

    public void SlideInUI()
    {
        titleUI.SetActive(true);
    }

        public void SlideOutUI()
    {
        titleUI.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
