﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour
{
    private Camera m_Camera;
    private void Start()
    {
        m_Camera = Camera.main;
    }
    //Orient the camera after all movement is completed this frame to avoid jittering
    void LateUpdate()
    {
        Vector3 v3T = Vector3.Lerp (transform.position , transform.position + m_Camera.transform.rotation * Vector3.forward, Time.deltaTime * 0.8f);
        v3T.y = transform.position.y;
        transform.LookAt(v3T, Vector3.up);
    }
}