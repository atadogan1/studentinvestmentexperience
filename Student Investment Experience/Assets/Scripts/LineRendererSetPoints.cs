﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererSetPoints : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    Transform point1, point2;
    Vector3[] points = new Vector3[2];
    LineRenderer lineRenderer;
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();

    }

    void Update()
    {
        points[0] = point1.position;
        points[1] = point2.position;
        lineRenderer.SetPositions(points);
    }
}
