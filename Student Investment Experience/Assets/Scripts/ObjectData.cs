﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjectData", menuName = "Assets/Create/ObjectData")]
public class ObjectData : ScriptableObject
{
    public string title, info1, info2, info3;
    public float kmsTraveled;
}
