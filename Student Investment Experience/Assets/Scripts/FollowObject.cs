﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    RectTransform rectTransform, targetTransform;
    // [SerializeField]
    float speed = 0.9f;
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        StartCoroutine(UnParent());
    }

    IEnumerator UnParent()
    {
        yield return null; 
        Vector3 temp = transform.position; // world pos
        transform.parent = null; // *should* not move, but you say...
        transform.position = temp; // restore world position
    }
    private void LateUpdate()
    {

        rectTransform.position = Vector3.Lerp(rectTransform.position, targetTransform.position, speed * Time.deltaTime);
        //     rectTransform.position = Vector3.Lerp(rectTransform.position, targetTransform.position, speed * Time.deltaTime);
    }

}