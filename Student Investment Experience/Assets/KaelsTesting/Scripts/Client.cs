﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;
using System.IO;
using System.Linq;

public class Client : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        EmissionsDataHelper dataHelper = new EmissionsDataHelper();
        Debug.Log(dataHelper.getEmissionsByFood("Beef"));
        Debug.Log(dataHelper.getSuppliedConsumptionByFood("Beef"));
    }
}