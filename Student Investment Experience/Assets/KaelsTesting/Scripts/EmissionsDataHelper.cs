﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;
using System.IO;

public class EmissionsDataHelper
{
    public Dictionary<string, Dictionary<string, string>> dict;

    public EmissionsDataHelper () {
        string filepath = "Assets\\KaelsTesting\\Scripts\\carbon_footprint_json.json";
        StreamReader reader = new StreamReader(filepath);
        string json = reader.ReadToEnd();
        Dictionary<string, object> bigDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        Dictionary<string, object> canadaData = JsonConvert.DeserializeObject<Dictionary<string, object>>(bigDict["Canada"].ToString());
        bigDict = canadaData;

        Dictionary<string, string> riceDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Rice"].ToString());
        Dictionary<string, string> wheatandproductsDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["WheatAndProducts"].ToString());
        Dictionary<string, string> fishDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Fish"].ToString());
        Dictionary<string, string> poultryDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Poultry"].ToString());
        Dictionary<string, string> milkandcheeseDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["MilkAndCheese"].ToString());
        Dictionary<string, string> nutsandpbDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["NutsAndPb"].ToString());
        Dictionary<string, string> eggsDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Eggs"].ToString());
        Dictionary<string, string> lambandgoatDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["LambAndGoat"].ToString());
        Dictionary<string, string> porkDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Pork"].ToString());
        Dictionary<string, string> soybeansDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Soybeans"].ToString());
        Dictionary<string, string> beefDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(bigDict["Beef"].ToString());

        this.dict = new Dictionary<string, Dictionary<string, string>>
        {
            ["Rice"] = riceDict,
            ["WheatAndProducts"] = wheatandproductsDict,
            ["Fish"] = fishDict,
            ["Poultry"] = poultryDict,
            ["MilkAndCheese"] = milkandcheeseDict,
            ["NutsAndPb"] = nutsandpbDict,
            ["Eggs"] = eggsDict,
            ["LambdaAndGoat"] = lambandgoatDict,
            ["Pork"] = porkDict,
            ["Soybeans"] = soybeansDict,
            ["Beef"] = beefDict
        };
    }

    public string getEmissionsByFood (string foodKey)
    {
        return this.dict[foodKey]["CO2KGEmission"];
    }

    public string getSuppliedConsumptionByFood (string foodkey)
    {
        return this.dict[foodkey]["SuppliedforConsumption"];
    }
}